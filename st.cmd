require essioc
require ecmccfg

epicsEnvSet IOC Stub-100:Ctrl-ECAT-001

iocshLoad("${essioc_DIR}/common_config.iocsh")

epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=8.0.2,NAMING=ESSnaming,EC_RATE=200,ECMC_ASYN_PORT_MAX_PARAMS=10000"
ecmcConfigOrDie "Cfg.SetTraceMaskBit(12,0)"

${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=0, HW_DESC=EK1100"

${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=1, HW_DESC=EL3202-0010"

# Configure channel 1 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
${SCRIPTEXEC} ${E3_CMD_TOP}/ecmcEL3202-0010-Sensor-chX_NCL.iocsh
# Configure channel 2 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
${SCRIPTEXEC} ${E3_CMD_TOP}/ecmcEL3202-0010-Sensor-chX_NCL.iocsh
dbLoadTemplate("$(E3_CMD_TOP)/db/Temperature_slave_1.substitutions","P=Stub-100:, ECMC_P=$(IOC), SLAVE_ID=$(ECMC_EC_SLAVE_NUM)")

${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=2, HW_DESC=EL3202-0010"

# Configure channel 1 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
${SCRIPTEXEC} ${E3_CMD_TOP}/ecmcEL3202-0010-Sensor-chX_NCL.iocsh
# Configure channel 2 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
${SCRIPTEXEC} ${E3_CMD_TOP}/ecmcEL3202-0010-Sensor-chX_NCL.iocsh
dbLoadTemplate("$(E3_CMD_TOP)/db/Temperature_slave_2.substitutions","P=Stub-100:, ECMC_P=$(IOC), SLAVE_ID=$(ECMC_EC_SLAVE_NUM)")

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

